# Fast cryptographic networking for Haskell

[NaCl][] (pronounced "salt") is a fast library for cryptographic
networking. Think of it like OpenSSL, but faster, easier, and more
secure.

These are the awesome Haskell bindings.

[travis-ci.org](http://travis-ci.org) results: [![Build Status](https://secure.travis-ci.org/thoughtpolice/salt.png?branch=master)](http://travis-ci.org/thoughtpolice/salt)

# Installation

See the [main page][] for installation instructions, dependencies,
documentation, and examples.

# Join in

File bugs in the GitHub [issue tracker][].

Master [git repository][gh]:

* `git clone https://github.com/thoughtpolice/salt.git`

There's also a [BitBucket mirror][bb]:

* `git clone https://bitbucket.org/thoughtpolice/salt.git`

# Authors

See [AUTHORS.txt](https://raw.github.com/thoughtpolice/salt/master/AUTHORS.txt).

# License

MIT. See [LICENSE.txt](https://raw.github.com/thoughtpolice/salt/master/LICENSE.txt) for terms of copyright and redistribution.

[NaCl]: http://nacl.cace-project.eu
[main page]: http://thoughtpolice.github.com/salt
[issue tracker]: http://github.com/thoughtpolice/salt/issues
[gh]: http://github.com/thoughtpolice/salt
[bb]: http://bitbucket.org/thoughtpolice/salt
